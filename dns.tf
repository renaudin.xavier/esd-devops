resource "scaleway_domain_record" "espace_sur_demande_anct" {
  provider = scaleway.scaleway_project
  dns_zone = var.prod_base_domain
  type     = "ALIAS"
  data     = "${var.production_cluster_cname}."
  ttl      = 3600
}

resource "scaleway_domain_record" "espace_sur_demande" {
  provider = scaleway.scaleway_project
  dns_zone = var.landing_page_domain
  type     = "ALIAS"
  data     = "${var.production_cluster_cname}."
  ttl      = 3600
}

resource "scaleway_domain_record" "espace_sur_demande_www" {
  provider = scaleway.scaleway_project
  dns_zone = var.landing_page_domain
  name     = "www"
  type     = "ALIAS"
  data     = "${var.production_cluster_cname}."
  ttl      = 3600
}

# mailjet config for espacesurdemande.fr

resource "scaleway_domain_record" "mailjet_auth_espace_sur_demande" {
  provider = scaleway.scaleway_project
  dns_zone = var.landing_page_domain
  type     = "TXT"
  name     = "mailjet._df93b79b"
  data     = "df93b79b852717237ad8aab61b95c8ba"
  ttl      = 3600
}
resource "scaleway_domain_record" "mailjet_spf_espace_sur_demande" {
  provider = scaleway.scaleway_project
  dns_zone = var.landing_page_domain
  type     = "TXT"
  data     = "v=spf1 include:spf.mailjet.com ?all"
  ttl      = 3600
}
resource "scaleway_domain_record" "mailjet_dkim_espace_sur_demande" {
  provider = scaleway.scaleway_project
  dns_zone = var.landing_page_domain
  type     = "TXT"
  name     = "mailjet._domainkey"
  data     = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9IztqoGhB4ykZkk6t2vO6cWPdCo2zSomfZzrZt8/dr+7kiGEVGNVz0awbUxk7gSMBk+H24F2qcGMBoR/d+167oBbi7yN7SCMCW8FgAOz3cM9MiKGrAm48pisoPtq3qen06OUX3aPjD+cqmPaKEDOTNsR79AwnlbQW/sOl+lbbdwIDAQAB"
  ttl      = 3600
}

# mailjet config for espacesurdemande.anct.gouv.fr

resource "scaleway_domain_record" "mailjet_auth_espace_sur_demande_anct_gouv_fr" {
  provider = scaleway.scaleway_project
  dns_zone = var.prod_base_domain
  type     = "TXT"
  name     = "mailjet._801bd7e1"
  data     = "801bd7e1721084b80938b6a36328046a"
}
resource "scaleway_domain_record" "mailjet_spf_espace_sur_demande_anct_gouv_fr" {
  provider = scaleway.scaleway_project
  dns_zone = var.prod_base_domain
  type     = "TXT"
  data     = "v=spf1 include:spf.mailjet.com ?all"
}
resource "scaleway_domain_record" "mailjet_dkim_espace_sur_demande_anct_gouv_fr" {
  provider = scaleway.scaleway_project
  dns_zone = var.prod_base_domain
  type     = "TXT"
  name     = "mailjet._domainkey"
  data     = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDGpKjJmGurn9QtHoLwUi1EaZrYrQSBVYssr/EBxfDlRgA+OqpV4oVkYFyPukLcLB9qPU4IEXzd01anA24vqNTqmRZtw0B6r+wumHiPkSVgJAgYKkKhgjUex2usQepFWFyXy+zQzWlcokpfw0BVSGiyXU7dV3YoW/him6Il+takiwIDAQAB"
}
