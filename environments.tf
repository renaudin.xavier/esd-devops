module "reviews" {
  source                   = "./environment"
  gitlab_environment_scope = "*"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  namespace                = "${var.project_slug}-reviews"
  gitlab_project_ids       = local.gitlab_project_ids
  project_slug             = var.project_slug
  project_name             = var.project_name
  scaleway_access_key      = var.scaleway_project_config.access_key
  scaleway_secret_key      = var.scaleway_project_config.secret_key

  namespace_quota_max_cpu_requests  = 4
  namespace_quota_max_memory_limits = "20Gi"

  monitoring_org_id = random_string.development_secret_org_id.result
  providers = {
    scaleway = scaleway.scaleway_project
  }
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  namespace                = "${var.project_slug}-development"
  gitlab_project_ids       = local.gitlab_project_ids
  project_slug             = var.project_slug
  project_name             = var.project_name
  scaleway_access_key      = var.scaleway_project_config.access_key
  scaleway_secret_key      = var.scaleway_project_config.secret_key
  backup_bucket            = scaleway_object_bucket.backup_bucket_development

  namespace_quota_max_cpu_requests  = 4
  namespace_quota_max_memory_limits = "20Gi"

  monitoring_org_id = random_string.development_secret_org_id.result
  providers = {
    scaleway = scaleway.scaleway_project
  }
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base_domain              = var.prod_base_domain
  landing_page_domain      = var.landing_page_domain
  namespace                = var.project_slug
  gitlab_project_ids       = local.gitlab_project_ids
  project_slug             = var.project_slug
  project_name             = var.project_name
  scaleway_access_key      = var.scaleway_project_config.access_key
  scaleway_secret_key      = var.scaleway_project_config.secret_key
  backup_bucket            = scaleway_object_bucket.backup_bucket_production

  namespace_quota_max_cpu_requests  = 4
  namespace_quota_max_memory_limits = "20Gi"

  monitoring_org_id = random_string.production_secret_org_id.result
  providers = {
    scaleway = scaleway.scaleway_project
  }
}
