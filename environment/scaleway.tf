resource "scaleway_object_bucket" "uploads" {
  name = "${var.project_slug}-${local.environment_slug}-uploads"
}
