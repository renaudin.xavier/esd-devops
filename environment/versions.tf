terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    helm = {
      source = "hashicorp/helm"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway
      ]
    }
  }
  required_version = ">= 0.14"
}
