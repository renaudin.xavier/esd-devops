resource "random_password" "reviews_postgresql_password" {
  count   = var.gitlab_environment_scope == "*" ? 1 : 0
  special = false
  length  = 64
}
locals {
  db_url      = var.gitlab_environment_scope == "*" ? "" : module.postgresql[0].uri
  db_backups  = var.gitlab_environment_scope != "*"
  db_replicas = var.gitlab_environment_scope != "*" ? 2 : 1
}

module "postgresql" {
  count   = var.gitlab_environment_scope == "*" ? 0 : 1
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name     = "postgresql"
  namespace      = module.namespace.namespace
  pg_volume_size = "10Gi"

  pg_replicas = local.db_replicas

  pg_backups_volume_enabled             = local.db_backups ? true : false
  pg_backups_volume_size                = local.db_backups ? "20Gi" : null
  pg_backups_volume_full_schedule       = local.db_backups ? "30 2 * * 0" : null
  pg_backups_volume_incr_schedule       = local.db_backups ? "30 2 * * 1-6" : null
  pg_backups_volume_full_retention      = local.db_backups ? 4 : null
  pg_backups_volume_full_retention_type = local.db_backups ? "count" : null

  pg_backups_s3_enabled       = local.db_backups ? true : false
  pg_backups_s3_bucket        = local.db_backups ? var.backup_bucket.name : null
  pg_backups_s3_region        = local.db_backups ? var.backup_bucket.region : null
  pg_backups_s3_endpoint      = local.db_backups ? "s3.fr-par.scw.cloud" : null
  pg_backups_s3_access_key    = local.db_backups ? var.scaleway_access_key : null
  pg_backups_s3_secret_key    = local.db_backups ? var.scaleway_secret_key : null
  pg_backups_s3_full_schedule = local.db_backups ? "30 3 * * 0" : null
  pg_backups_s3_incr_schedule = local.db_backups ? "30 3 * * 1-6" : null

  values = [<<-EOT
    postgresVersion: 14
    postGISVersion: 3.2
    imagePostgres: null
    imagePgBackRest: null
    EOT
    , file("${path.module}/db_resources.yaml")
  ]
}
