module "redirect_www" {
  count   = var.gitlab_environment_scope == "production" ? 1 : 0
  source  = "gitlab.com/vigigloo/tools-k8s/nginxredirect"
  version = "0.1.0"

  chart_name    = "redirection"
  chart_version = "0.1.0"
  namespace     = module.namespace.namespace

  values = [
    <<-EOT
    ingress:
      className: null
      enabled: true
      certManagerClusterIssuer: letsencrypt-prod
      host: null
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
    EOT
  ]
  redirect_from = "www.${local.landing_page_domain}"
  redirect_to   = "https://${local.landing_page_domain}"

  requests_cpu    = "10m"
  requests_memory = null
  limits_cpu      = null
  limits_memory   = "40Mi"
}

moved {
  from = module.redirect_www
  to   = module.redirect_www[0]
}
