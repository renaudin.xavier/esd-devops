module "kubeconfig_jonathan_platteau" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "1.0.0"

  filename               = "jonathan-platteau-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "jonathan-platteau"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}

module "kubeconfig_sylvain_le_gleau" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "1.0.0"

  filename               = "sylvain-le-gleau-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "sylvain-le-gleau"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}

module "kubeconfig_xavier_renaudin" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "1.0.0"

  filename               = "xavier-renaudin-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "xavier-renaudin"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
